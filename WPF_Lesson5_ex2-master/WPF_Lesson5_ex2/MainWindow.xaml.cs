﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF_Lesson5_ex2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private List<SolidColorBrush> m_brushes;

        public MainWindow()
        {
            InitializeComponent();

            m_brushes = new List<SolidColorBrush>();
            m_brushes.Add(Brushes.AliceBlue);
            m_brushes.Add(Brushes.MediumSpringGreen);
            m_brushes.Add(Brushes.Violet);
            m_brushes.Add(Brushes.HotPink);
            m_brushes.Add(Brushes.Fuchsia);
            m_brushes.Add(Brushes.Gold);
            m_brushes.Add(Brushes.YellowGreen);
            m_brushes.Add(Brushes.DarkViolet);
            m_brushes.Add(Brushes.LightPink);
            m_brushes.Add(Brushes.BurlyWood);

            Task.Run(() =>
            {
                while (true)
                {
                    SafeInvoker.SafeInvoke(this, ChangeColor);
                    Thread.Sleep(random.Next(50, 150));
                }
            });

        }
        static Random random = new Random();

        private void ChangeColor()
        {
            brdr1.BorderBrush = m_brushes[random.Next(10)];
            brdr2.BorderBrush = m_brushes[random.Next(10)];
            brdr3.BorderBrush = m_brushes[random.Next(10)];
        }

        private void SafeInvoke(Action work)
        {
            if (Dispatcher.CheckAccess())
            {
                work.Invoke();
                return;
            }
            this.Dispatcher.BeginInvoke(work);
        }
    }
}
