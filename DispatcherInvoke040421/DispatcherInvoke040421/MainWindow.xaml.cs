﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DispatcherInvoke040421
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        // 1
        //private async void startBtn_Click(object sender, RoutedEventArgs e)
        //{
        //    //00:00:00.00
        //    while (true)
        //    {
        //        timeLbl.Content = DateTime.Now.TimeOfDay;
        //        await Task.Run(()=>{ Thread.Sleep(5); });
        //    }
        //}

        private void SafeInvoke(Action uiWork)
        {
            // check the current hread.
            // if its UI thead then execute uiWork
            // if not, then summon dispatcher.invoke (uiWork)
            if (Dispatcher.CheckAccess()) // am I the UI thread?
            {
                uiWork.Invoke();
                return;
            }
            this.Dispatcher.BeginInvoke(uiWork); // calls the UI thread to draw!
        }

        private void PrintTime()
        {
            timeLbl.Content = DateTime.Now.TimeOfDay;
        }

        private void startBtn_Click(object sender, RoutedEventArgs e)
        {
            //00:00:00.00
            Task.Run(() =>
            {
                while (true)
                {
                    // ERROR
                    //timeLbl.Content = DateTime.Now.TimeOfDay;

                    // call UI thread
                    //Action uiWork = () => { timeLbl.Content = DateTime.Now.TimeOfDay; };
                    //this.Dispatcher.BeginInvoke(uiWork); // calls the UI thread to draw!

                    SafeInvoker.SafeInvoke(this, PrintTime);
                    Thread.Sleep(1); 
                }
            });
            startBtn.IsEnabled = false;
        }

        private void timeLbl_MouseMove(object sender, MouseEventArgs e)
        {
            SafeInvoker.SafeInvoke(this, PrintTime);
        }
    }
}
