﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfDataContext.DataContext
{
    public class MainWindowDataContext
    {
        private static Random random = new Random();

        public int Number { get; set; }
        public int Number2 { get; set; }

        public MainWindowDataContext()
        {
            Number = random.Next(100, 200);
            Number2 = random.Next(100, 200);
        }
    }
}
