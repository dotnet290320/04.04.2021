﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfDataContext.Model;

namespace WpfDataContext
{
    public class PersonDataContext
    {
        public Person MyPerson { get; set; }

        public PersonDataContext()
        {
            MyPerson = new Person
            {
                Name = "Gili",
                Age = 22
            };
        }

    }
}
