﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfDataContext.DataContext;

namespace WpfDataContext
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        PersonDataContext m_personDataContext = new PersonDataContext();
        MainWindowDataContext m_mainWindowDataContext = new MainWindowDataContext();

        public MainWindow()
        {
            InitializeComponent();

            // this.DataContext = this; // not good
            this.DataContext = m_mainWindowDataContext;
            mylbl.DataContext = m_personDataContext;
            //mylbl2.DataContext = m_mainWindowDataContext;

            // add class MobilePhone { Model, Brand, Color }
            // add class called : MobileDataContext
            // add 3 fields { Model, Brand, Color } to page
            // Add INotifyPropertyChange to Number property
            // button should change the number value

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //m_personDataContext.MyPerson.Name = "new name... " + DateTime.Now;

            // will not appear
            m_personDataContext.MyPerson = new Model.Person()
            {
                Name = "A",
                Age = 1
            };
        }
    }
}
