﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfDataContext.Model
{
    public class Person : INotifyPropertyChanged
    {
        private string m_name;
        public string Name
        {
            get
            {
                return this.m_name;
            }
            set
            {
                this.m_name = value;
                OnPropertyChanged("Name");
            }
        }
        public int Age { get; set; }

        private void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public override string ToString()
        {
            return $"Person name {Name} {Age}";
        }
    }
}
